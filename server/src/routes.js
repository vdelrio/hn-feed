const express = require("express");
const articleRepository = require("./repos/article.repository");
const router = express.Router();

const DEFAULT_ARTICLES_LIMIT = 100;

// Get all articles
router.get('/articles', (req, res) => {
  const { limit } = req.query;
  articleRepository
    .findAll(parseInt(limit) || DEFAULT_ARTICLES_LIMIT)
    .then((articles) => res.send(articles));
});

// Delete article
router.delete('/articles/:objectId', async (req, res) => {
  const { objectId } = req.params;
  await articleRepository.remove(objectId);

  res.send('The article was successfully deleted');
});

module.exports = router
