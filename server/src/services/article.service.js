const axios = require('axios');
const articleRepository = require('../repos/article.repository')

const HACKER_NEWS_PAGE_SIZE = 100;
const HACKER_NEWS_URL = `https://hn.algolia.com/api/v1/search_by_date?query=nodejs&tags=story&hitsPerPage=${HACKER_NEWS_PAGE_SIZE}`;
// Search up to 2 hours back (just in case)
const SINCE_HOURS_AGO = 2;


const mapStoryToArticle = (story) => ({
  objectId: story.objectID,
  title: story.story_title || story.title,
  author: story.author,
  createdAt: story.created_at,
  url: story.story_url || story.url,
  deleted: false,
});

const getHackerNewsUrl = async () => {
  let url = HACKER_NEWS_URL;
  const articlesAmount = await articleRepository.count();
  if (articlesAmount > 0) {
    const now = Math.floor(new Date().getTime() / 1000);
    url += `&numericFilters=created_at_i>${now - SINCE_HOURS_AGO * 3600}`;
  }
  return url;
};

const getAndStoreArticles = async () => {
  const url = await getHackerNewsUrl();
  let currentPage = 0;
  let totalPages = 0;
  let articles = [];

  do {
    const finalUrl = `${url}&page=${currentPage}`;
    console.log(`Fetching stories at ${finalUrl}`);

    const resp = await axios.get(finalUrl);
    totalPages = resp.data.nbPages;

    const titledStories = resp.data.hits.filter(
      (story) => story.story_title || story.title
    );
    articles = articles.concat(
      titledStories.map((story) => mapStoryToArticle(story))
    );
    currentPage += 1;
  } while (currentPage < totalPages - 1);

  if (articles.length > 0) {
    articleRepository.insertMany(articles);
  }
  return articles;
};

exports.getAndStoreArticles = getAndStoreArticles
