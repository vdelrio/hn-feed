const mongoose = require('mongoose');

const schema = mongoose.Schema({
  objectId: { type: String, index: true },
  title: String,
  author: String,
  createdAt: String,
  url: String,
  deleted: { type: Boolean, default: false }
});

module.exports = mongoose.model('Article', schema);
