/* eslint no-undef: 0 */
const request = require('supertest');
const app = require('../app')
const mongoose = require("mongoose");
const env = require('../../env');
const mockedArticles = require('./mockedArticles');
const Article = require('../models/Article');
const mongooseConnect = require('../mongooseConnect');

beforeAll(async () => {
  await mongooseConnect(env.TEST_DB_CONNECTION_STRING,);
  await Article.deleteMany();
  await Article.insertMany(mockedArticles);
});

// afterEach(async () => {
//   await Article.deleteMany();
// });

afterAll(async () => {
  await mongoose.connection.close();
});

describe('Endpoints', () => {

  it('Not Found', async () => {
    const response = await request(app).get('/non-existent');
    expect(response.status).toBe(404);
    expect(response.body.message).toBe('Not Found')
  });

  it('Article list', async () => {
    const response = await request(app).get('/api/v1/articles');
    expect(response.status).toBe(200);
    expect(response.body.length).toBe(10);
  });

  it('Limited article list', async () => {
    const response = await request(app).get('/api/v1/articles?limit=3');
    expect(response.status).toBe(200);
    expect(response.body.length).toBe(3);
  });

  it('Delete article', async () => {
    const ARTICLE_ID = 23670625;
    let article = await Article.findOne({ objectId: ARTICLE_ID })
    expect(article.deleted).toBe(false);

    const response = await request(app).delete(`/api/v1/articles/${ARTICLE_ID}`);
    expect(response.status).toBe(200);

    article = await Article.findOne({ objectId: ARTICLE_ID })
    expect(article.deleted).toBe(true);
  });

});
