const express = require('express');
const cors = require('cors');
const routes = require('./routes');
const app = express();

// Enable cors
app.use(cors());

// Add routes
app.use('/api/v1', routes)

// Not found
app.use((req, res) => {
  res.status(404).send({ message: 'Not Found' });
});

module.exports = app;
