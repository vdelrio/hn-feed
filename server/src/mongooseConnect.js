const mongoose = require('mongoose');

const mongooseConnect = (uri) => mongoose.connect(
  uri,
  {
    useNewUrlParser: true,
    useCreateIndex: true,
  }
).catch(err => console.log(err));

module.exports = mongooseConnect
