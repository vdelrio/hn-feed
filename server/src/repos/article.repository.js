const Article = require('../models/Article')

const DB_ERROR_CODE_DUPLICATED_KEY = 11000;


const findAll = (limit) => (
  Article
    .find({ deleted: false })
    .limit(limit)
)

const insertMany = (articles) => {
  Article
    .insertMany(articles, { ordered: false }).catch((err) => {
    // We ignore duplicated key error
    if (err.code !== DB_ERROR_CODE_DUPLICATED_KEY) {
      console.error(`Failed to insert documents: ${err}`);
    }
  });
};

const remove = (objectId) => (
  Article.updateOne({ objectId }, { deleted: true })
)

const count = () => (
  Article.countDocuments()
)

exports.findAll = findAll;
exports.insertMany = insertMany;
exports.remove = remove;
exports.count = count;
