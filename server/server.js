const mongooseConnect = require('./src/mongooseConnect')
const CronJob = require('cron').CronJob
const env = require('./env')
const app = require('./src/app')
const articleService = require('./src/services/article.service')

// Connect to MongoDB database
mongooseConnect(env.DB_CONNECTION_STRING)
  .then(() => {
    app.listen(env.PORT, () =>
      console.log(`Listening at http://localhost:${env.PORT}`)
    );
  })

new CronJob(
  '0 * * * *',
  () => {
    mongooseConnect(env.DB_CONNECTION_STRING)
      .then(() => {
        articleService.getAndStoreArticles()
          .catch((error) =>
            console.log(error)
          );
      })
      .catch((error) => console.error(error));
  },
  null,
  true,
  'America/Los_Angeles',
  null,
  true
);
