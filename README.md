[![pipeline status](https://gitlab.com/vdelrio/hn-feed/badges/master/pipeline.svg)](https://gitlab.com/vdelrio/hn-feed/-/commits/master)

# HN Feed
Read your favorites articles about Node.js on Hacker News.

## Run app
In order to execute the application you have to execute the following command:
```
docker-compose up
``` 
And you can stop it with:
```
docker-compose down
```

## Initialization
There is no need for any manual initialization, you just visit http://localhost in your browser.

## Run tests
You can execute the tests and see the coverage with the following command:
```
docker-compose --file docker-compose.test.yml up
```
Once you see the results you can exit the execution as follow:
```
docker-compose down
```
