import React from 'react';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Header from './components/Header';
import ArticleList from './components/ArticleList';

const App = () => (
  <Container maxWidth={false} disableGutters>
    <Box mb={4}>
      <Header
        title="HN Feed"
        subtitle="We <3 hacker news!"
      />
      <ArticleList />
    </Box>
  </Container>
);

export default App;
