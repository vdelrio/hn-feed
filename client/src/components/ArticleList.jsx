import React, { useState, useEffect } from 'react';
import axios from 'axios';
import moment from 'moment';
import DeleteIcon from '@material-ui/icons/Delete';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { GET_ARTICLES_URL, DELETE_ARTICLE_URL } from '../config';

const useStyles = makeStyles({
  tableContainer: {
    padding: '20px 40px',
    boxSizing: 'border-box',
  },
  title: {
    color: '#333',
  },
  time: {
    color: '#333',
  },
  author: {
    color: '#999',
  },
});

const StyledTableRow = withStyles({
  root: {
    '&:hover': {
      backgroundColor: '#fafafa !important',
      cursor: 'pointer',
    },
  },
})(TableRow);

const StyledTableCell = withStyles({
  body: {
    padding: '5px 20px',
    borderBottom: '1px solid #ccc',
    fontSize: '13pt',
  },
})(TableCell);

const ArticleList = () => {
  const classes = useStyles();
  const [articles, setArticles] = useState([]);

  useEffect(() => {
    axios.get(GET_ARTICLES_URL)
      .then((res) => setArticles(res.data));
  }, []);

  const formatCreationDate = (strDate) => {
    const today = moment();
    const yesterday = today.clone().subtract(1, 'day');
    const date = moment(strDate);

    if (date.isSame(today, 'd')) {
      return date.format('h:mm a');
    }
    if (date.isSame(yesterday, 'd')) {
      return 'Yesterday';
    }
    if (date.isSame(today, 'y')) {
      return date.format('MMM DD');
    }
    return date.format('MMM DD, YYYY');
  };

  const deleteArticle = (objectId) => {
    axios.delete(`${DELETE_ARTICLE_URL}/${objectId}`)
      .then(() => (
        setArticles((prevState) => prevState.filter((article) => article.objectId !== objectId))
      ));
  };

  return (
    <TableContainer className={classes.tableContainer}>
      <Table aria-label="articles">
        <TableBody>
          {articles.map((article) => (
            <StyledTableRow key={article.objectId} hover>
              <StyledTableCell onClick={() => window.open(article.url, '_blank')}>
                <span className={classes.title}>{article.title}</span>
                {' '}
                <span className={classes.author}>- {article.author} -</span>
              </StyledTableCell>
              <StyledTableCell className={classes.time}>
                {formatCreationDate(article.createdAt)}
              </StyledTableCell>
              <StyledTableCell>
                <IconButton
                  aria-label="delete"
                  onClick={() => deleteArticle(article.objectId)}
                  className="delete-btn"
                >
                  <DeleteIcon />
                </IconButton>
              </StyledTableCell>
            </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default ArticleList;
