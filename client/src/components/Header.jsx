import React from 'react';
import PropTypes from 'prop-types';
import './Header.css';

const Header = ({ title, subtitle }) => (
  <div className="header">
    <div className="title">{title}</div>
    {subtitle && (
      <div className="subtitle">{subtitle}</div>
    )}
  </div>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
};

Header.defaultProps = {
  subtitle: '',
};

export default Header;
