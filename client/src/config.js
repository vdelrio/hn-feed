export const API_BASE_URL = 'http://localhost:8000/api/v1';
export const ARTICLES_AMOUNT = 100;
export const GET_ARTICLES_URL = `${API_BASE_URL}/articles/?limit=${ARTICLES_AMOUNT}`;
export const DELETE_ARTICLE_URL = `${API_BASE_URL}/articles`;
